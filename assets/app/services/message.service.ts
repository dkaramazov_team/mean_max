import { ErrorService } from '../errors/error.service';
import { Observable } from 'rxjs/Rx';
import { Headers, Http, Response } from '@angular/http';
import { Message } from '../messages/message.model';
import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/Rx';

@Injectable()
export class MessageService{
    messages: Message[] = [];
    messageIsEdit = new EventEmitter();
    headers: Headers = new Headers({'Content-Type': 'application/json'});
    url: string = 'http://www.opendoortrips.com/';


    constructor(private http: Http, private errorService: ErrorService){}

    addMessage(message: Message){
        const token = localStorage.getItem('token') ? `?token=${localStorage.getItem('token')}`:'';
        return this.http.post(this.url + `message${token}`, JSON.stringify(message), {headers: this.headers})
            .map((response: Response) => {
                const result = response.json();
                const message = new Message(
                    result.obj.content, 
                    result.obj.user.firstName, 
                    result.obj._id, 
                    result.obj.user._id
                );
                this.messages.push(message);
                return message;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            }
        );
    }

    getMessages(){
        return this.http.get(this.url + 'message')
            .map((response: Response) => {
                const messages = response.json().obj;
                let transformedMessages: Message[] = [];
                for(let message of messages){
                    transformedMessages.push(new Message(
                        message.content, 
                        message.user.firstName, 
                        message._id, 
                        message.user._id)
                    );
                }
                this.messages = transformedMessages;
                return transformedMessages;
            })
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    editMessage(message: Message){
        this.messageIsEdit.emit(message);
    }

    updateMessage(message: Message){
        const token = localStorage.getItem('token') ? `?token=${localStorage.getItem('token')}`:'';        
        return this.http.patch(this.url + `message/${message.messageId}${token}`, JSON.stringify(message), {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    deleteMessage(message: Message){
        this.messages.splice(this.messages.indexOf(message), 1);
        const token = localStorage.getItem('token') ? `?token=${localStorage.getItem('token')}`:'';        
        return this.http.delete(this.url + `message/${message.messageId}${token}`, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
    
}