import { ErrorService } from '../errors/error.service';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response } from '@angular/http';
import { User } from '../auth/user.model';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService{
    headers: Headers = new Headers({'Content-Type': 'application/json'});
    url: string = 'http://www.opendoortrips.com/';


    constructor(private http: Http, private errorService: ErrorService){}

    signup(user: User){
        return this.http.post(this.url + 'user', JSON.stringify(user), { headers: this.headers })
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }
    
    signin(user: User){
        return this.http.post(this.url + 'user/signin', JSON.stringify(user), { headers: this.headers })
            .map((response: Response) => response.json())
            .catch((error: Response) => {
                this.errorService.handleError(error.json());
                return Observable.throw(error.json());
            });
    }

    logout(){
        localStorage.clear();
    }

    isLoggedIn(){
        return localStorage.getItem('token') !== null;
    }

}