import { MessageService } from '../services/message.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message.component';
import { MessageListComponent } from './message-list.component';
import { MessageInputComponent } from './message-input.component';
import { MessagesComponent } from './messages.component';
import { NgModule } from '@angular/core';
@NgModule({
    declarations: [
        MessagesComponent,
        MessageInputComponent,
        MessageListComponent,
        MessageComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    providers: [MessageService]
})
export class MessageModule{

}