/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as i0 from '@angular/core';
import * as i1 from './messages.module';
import * as i2 from '@angular/common';
import * as i3 from '@angular/forms';
import * as i4 from '../services/message.service';
import * as i5 from '@angular/http';
import * as i6 from '../errors/error.service';
export const MessageModuleNgFactory:i0.NgModuleFactory<i1.MessageModule> = i0.ɵcmf(i1.MessageModule,
    ([] as any[]),(_l:any) => {
      return i0.ɵmod([i0.ɵmpd(512,i0.ComponentFactoryResolver,i0.ɵCodegenComponentFactoryResolver,
          [[8,([] as any[])],[3,i0.ComponentFactoryResolver],i0.NgModuleRef]),i0.ɵmpd(4608,
          i2.NgLocalization,i2.NgLocaleLocalization,[i0.LOCALE_ID]),i0.ɵmpd(4608,i3.ɵi,
          i3.ɵi,([] as any[])),i0.ɵmpd(4608,i4.MessageService,i4.MessageService,[i5.Http,
          i6.ErrorService]),i0.ɵmpd(512,i2.CommonModule,i2.CommonModule,([] as any[])),
          i0.ɵmpd(512,i3.ɵba,i3.ɵba,([] as any[])),i0.ɵmpd(512,i3.FormsModule,i3.FormsModule,
              ([] as any[])),i0.ɵmpd(512,i1.MessageModule,i1.MessageModule,([] as any[]))]);
    });
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiQzovVXNlcnMvanBoaXBwcy9EZXNrdG9wL0FuZ3VsYXIgVHV0b3JpYWxzL01heC9zZWVkLXByb2plY3Qvc2VlZC1wcm9qZWN0L2Fzc2V0cy9hcHAvbWVzc2FnZXMvbWVzc2FnZXMubW9kdWxlLm5nZmFjdG9yeS50cyIsInZlcnNpb24iOjMsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nOi8vL0M6L1VzZXJzL2pwaGlwcHMvRGVza3RvcC9Bbmd1bGFyIFR1dG9yaWFscy9NYXgvc2VlZC1wcm9qZWN0L3NlZWQtcHJvamVjdC9hc3NldHMvYXBwL21lc3NhZ2VzL21lc3NhZ2VzLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyIgIl0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9
