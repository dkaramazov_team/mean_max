import { NgForm } from '@angular/forms/src/directives';
import { MessageService } from '../services/message.service';
import { Message } from './message.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
    selector: 'app-message-input',
    templateUrl: './message-input.component.html'
})
export class MessageInputComponent implements OnInit{
    message: Message;

    constructor(private messageService: MessageService){

    }

    ngOnInit(){
        this.messageService.messageIsEdit.subscribe(
            (message: Message) => this.message = message
        )
    }

    onClear(form: NgForm){
        form.resetForm();
        this.message = null;
    }

    onSubmit(form: NgForm){
        if(this.message){
            this.message.content = form.value.content;
            this.messageService.updateMessage(this.message)
                .subscribe(
                    result => console.log(result)
                );
            this.message = null;
        } else{
            const message = new Message(form.value.content, 'Max');
            this.messageService.addMessage(message)
                .subscribe(
                    data => console.log(data),
                    error => console.log(error)
                );
        }
        form.resetForm();
    }
}