import { MessageService } from '../services/message.service';

import { Message } from './message.model';
import { Component, Input, OnInit, Output } from '@angular/core';
@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styles: [`
        .col-md-8.col-md-offset-2{

        }
    `]
})
export class MessageListComponent implements OnInit{
    messages: Message[] = [
        new Message('Some message', 'Max'),
        new Message('Some message', 'Max'),
        new Message('Some message', 'Max')
    ];
    
    constructor(private messageService: MessageService){

    }

    ngOnInit(){
        this.messageService.getMessages().subscribe(
            (messages: Message[]) => {
                this.messages = messages;
            }
        )
    }

}