import { Error } from './error.model';
import { Injectable, EventEmitter } from '@angular/core';
@Injectable()
export class ErrorService{
    errorOccurred = new EventEmitter<Error>();

    handleError(error: Error){
        const errorData = new Error(error.title, error.message);
        this.errorOccurred.emit(errorData);
    }
}