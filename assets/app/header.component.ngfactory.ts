/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as i0 from '@angular/core';
import * as i1 from '@angular/router';
import * as i2 from '@angular/common';
import * as i3 from './header.component';
const styles_HeaderComponent:any[] = [''];
export const RenderType_HeaderComponent:i0.RendererType2 = i0.ɵcrt({encapsulation:0,
    styles:styles_HeaderComponent,data:{}});
export function View_HeaderComponent_0(_l:any):i0.ɵViewDefinition {
  return i0.ɵvid(0,[(_l()(),i0.ɵeld(0,(null as any),(null as any),25,'header',[['class',
      'row']],(null as any),(null as any),(null as any),(null as any),(null as any))),
      (_l()(),i0.ɵted((null as any),['\n    '])),(_l()(),i0.ɵeld(0,(null as any),(null as any),
          22,'nav',[['class','col-md-8 col-md-offset-2']],(null as any),(null as any),
          (null as any),(null as any),(null as any))),(_l()(),i0.ɵted((null as any),
          ['\n        '])),(_l()(),i0.ɵeld(0,(null as any),(null as any),19,'ul',[['class',
          'nav nav-pills']],(null as any),(null as any),(null as any),(null as any),
          (null as any))),(_l()(),i0.ɵted((null as any),['\n            '])),(_l()(),
          i0.ɵeld(0,(null as any),(null as any),7,'li',[['routerLinkActive','active']],
              (null as any),(null as any),(null as any),(null as any),(null as any))),
      i0.ɵdid(1720320,(null as any),2,i1.RouterLinkActive,[i1.Router,i0.ElementRef,
          i0.Renderer2,i0.ChangeDetectorRef],{routerLinkActive:[0,'routerLinkActive']},
          (null as any)),i0.ɵqud(603979776,1,{links:1}),i0.ɵqud(603979776,2,{linksWithHrefs:1}),
      (_l()(),i0.ɵeld(0,(null as any),(null as any),3,'a',([] as any[]),[[1,'target',
          0],[8,'href',4]],[[(null as any),'click']],(_v,en,$event) => {
        var ad:boolean = true;
        if (('click' === en)) {
          const pd_0:any = ((<any>i0.ɵnov(_v,11).onClick($event.button,$event.ctrlKey,
              $event.metaKey,$event.shiftKey)) !== false);
          ad = (pd_0 && ad);
        }
        return ad;
      },(null as any),(null as any))),i0.ɵdid(671744,[[2,4]],0,i1.RouterLinkWithHref,
          [i1.Router,i1.ActivatedRoute,i2.LocationStrategy],{routerLink:[0,'routerLink']},
          (null as any)),i0.ɵpad(1),(_l()(),i0.ɵted((null as any),['Messenger'])),
      (_l()(),i0.ɵted((null as any),['\n            '])),(_l()(),i0.ɵeld(0,(null as any),
          (null as any),7,'li',[['routerLinkActive','active']],(null as any),(null as any),
          (null as any),(null as any),(null as any))),i0.ɵdid(1720320,(null as any),
          2,i1.RouterLinkActive,[i1.Router,i0.ElementRef,i0.Renderer2,i0.ChangeDetectorRef],
          {routerLinkActive:[0,'routerLinkActive']},(null as any)),i0.ɵqud(603979776,
          3,{links:1}),i0.ɵqud(603979776,4,{linksWithHrefs:1}),(_l()(),i0.ɵeld(0,(null as any),
          (null as any),3,'a',([] as any[]),[[1,'target',0],[8,'href',4]],[[(null as any),
              'click']],(_v,en,$event) => {
            var ad:boolean = true;
            if (('click' === en)) {
              const pd_0:any = ((<any>i0.ɵnov(_v,20).onClick($event.button,$event.ctrlKey,
                  $event.metaKey,$event.shiftKey)) !== false);
              ad = (pd_0 && ad);
            }
            return ad;
          },(null as any),(null as any))),i0.ɵdid(671744,[[4,4]],0,i1.RouterLinkWithHref,
          [i1.Router,i1.ActivatedRoute,i2.LocationStrategy],{routerLink:[0,'routerLink']},
          (null as any)),i0.ɵpad(1),(_l()(),i0.ɵted((null as any),['Authentication'])),
      (_l()(),i0.ɵted((null as any),['\n        '])),(_l()(),i0.ɵted((null as any),
          ['\n    '])),(_l()(),i0.ɵted((null as any),['\n']))],(_ck,_v) => {
    const currVal_0:any = 'active';
    _ck(_v,7,0,currVal_0);
    const currVal_3:any = _ck(_v,12,0,'/messages');
    _ck(_v,11,0,currVal_3);
    const currVal_4:any = 'active';
    _ck(_v,16,0,currVal_4);
    const currVal_7:any = _ck(_v,21,0,'/auth');
    _ck(_v,20,0,currVal_7);
  },(_ck,_v) => {
    const currVal_1:any = i0.ɵnov(_v,11).target;
    const currVal_2:any = i0.ɵnov(_v,11).href;
    _ck(_v,10,0,currVal_1,currVal_2);
    const currVal_5:any = i0.ɵnov(_v,20).target;
    const currVal_6:any = i0.ɵnov(_v,20).href;
    _ck(_v,19,0,currVal_5,currVal_6);
  });
}
export function View_HeaderComponent_Host_0(_l:any):i0.ɵViewDefinition {
  return i0.ɵvid(0,[(_l()(),i0.ɵeld(0,(null as any),(null as any),1,'app-header',([] as any[]),
      (null as any),(null as any),(null as any),View_HeaderComponent_0,RenderType_HeaderComponent)),
      i0.ɵdid(49152,(null as any),0,i3.HeaderComponent,([] as any[]),(null as any),
          (null as any))],(null as any),(null as any));
}
export const HeaderComponentNgFactory:i0.ComponentFactory<i3.HeaderComponent> = i0.ɵccf('app-header',
    i3.HeaderComponent,View_HeaderComponent_Host_0,{},{},([] as any[]));
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiQzovVXNlcnMvanBoaXBwcy9EZXNrdG9wL0FuZ3VsYXIgVHV0b3JpYWxzL01heC9zZWVkLXByb2plY3Qvc2VlZC1wcm9qZWN0L2Fzc2V0cy9hcHAvaGVhZGVyLmNvbXBvbmVudC5uZ2ZhY3RvcnkudHMiLCJ2ZXJzaW9uIjozLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZzovLy9DOi9Vc2Vycy9qcGhpcHBzL0Rlc2t0b3AvQW5ndWxhciBUdXRvcmlhbHMvTWF4L3NlZWQtcHJvamVjdC9zZWVkLXByb2plY3QvYXNzZXRzL2FwcC9oZWFkZXIuY29tcG9uZW50LnRzIiwibmc6Ly8vQzovVXNlcnMvanBoaXBwcy9EZXNrdG9wL0FuZ3VsYXIgVHV0b3JpYWxzL01heC9zZWVkLXByb2plY3Qvc2VlZC1wcm9qZWN0L2Fzc2V0cy9hcHAvaGVhZGVyLmNvbXBvbmVudC5odG1sIiwibmc6Ly8vQzovVXNlcnMvanBoaXBwcy9EZXNrdG9wL0FuZ3VsYXIgVHV0b3JpYWxzL01heC9zZWVkLXByb2plY3Qvc2VlZC1wcm9qZWN0L2Fzc2V0cy9hcHAvaGVhZGVyLmNvbXBvbmVudC50cy5IZWFkZXJDb21wb25lbnRfSG9zdC5odG1sIl0sInNvdXJjZXNDb250ZW50IjpbIiAiLCI8aGVhZGVyIGNsYXNzPVwicm93XCI+XHJcbiAgICA8bmF2IGNsYXNzPVwiY29sLW1kLTggY29sLW1kLW9mZnNldC0yXCI+XHJcbiAgICAgICAgPHVsIGNsYXNzPVwibmF2IG5hdi1waWxsc1wiPlxyXG4gICAgICAgICAgICA8bGkgcm91dGVyTGlua0FjdGl2ZT1cImFjdGl2ZVwiPjxhIFtyb3V0ZXJMaW5rXT1cIlsnL21lc3NhZ2VzJ11cIj5NZXNzZW5nZXI8L2E+PC9saT5cclxuICAgICAgICAgICAgPGxpIHJvdXRlckxpbmtBY3RpdmU9XCJhY3RpdmVcIj48YSBbcm91dGVyTGlua109XCJbJy9hdXRoJ11cIj5BdXRoZW50aWNhdGlvbjwvYT48L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICA8L25hdj5cclxuPC9oZWFkZXI+IiwiPGFwcC1oZWFkZXI+PC9hcHAtaGVhZGVyPiJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7b0JDQUE7TUFBQTtNQUFvQiwyQ0FDaEI7VUFBQTtVQUFBLDRDQUFzQztVQUFBLGlCQUNsQztVQUFBO1VBQUEsZ0JBQTBCLG1EQUN0QjtpQkFBQTtjQUFBO2FBQUE7MkNBQUE7VUFBQTtNQUE4QjtVQUFBO1FBQUE7UUFBQTtVQUFBO2NBQUE7VUFBQTtRQUFBO1FBQUE7TUFBQSx1Q0FBQTtVQUFBO1VBQUEsc0JBQUcsSUFBNkI7TUFBa0IsbURBQ2hGO1VBQUE7VUFBQSxtREFBQTtVQUFBO1VBQUE7VUFBQSxxREFBOEI7VUFBQTtjQUFBO1lBQUE7WUFBQTtjQUFBO2tCQUFBO2NBQUE7WUFBQTtZQUFBO1VBQUEsdUNBQUE7VUFBQTtVQUFBLHNCQUFHLElBQXlCO01BQXVCLCtDQUNoRjtVQUFBLGFBQ0g7SUFITTtJQUFKLFdBQUksU0FBSjtJQUFpQztJQUFILFlBQUcsU0FBSDtJQUMxQjtJQUFKLFlBQUksU0FBSjtJQUFpQztJQUFILFlBQUcsU0FBSDs7SUFEQTtJQUFBO0lBQUEsWUFBQSxtQkFBQTtJQUNBO0lBQUE7SUFBQSxZQUFBLG1CQUFBOzs7O29CQ0oxQztNQUFBO2FBQUE7VUFBQTs7OyJ9
